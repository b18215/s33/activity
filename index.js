/*Express commands:

	To install express package
		1. npm init -y
		2. npm i express or npm install express
		3. touch .gitignore (to ignore the files in the node module folder as it will take time to push the files)


other commands:
	To uninstall package:
		1. npm uninstall express
		2. npm i express@4.18.1 (to be used if some package are not working with the latest version of express JS)


*/


// Load the expressjs module into our application and saved it in a variable called express
const express = require("express");


// This creates an application that uses express and store it as app. App is our server
const app = express();

const port = 4000;

//midleware
app.use(express.json())


// Mock-data
let users = [
	{
		username: "BMadrigal",
		email: "fatereader@gmail.com",
		password: "dontTalAboutMe"
	},
	{
		username: "Luisa",
		email: "stronggirl@gmail.com",
		password: "pressure"
	},

];

let items = [
	{
		name: "roses",
		price: 170,
		isActive: true
	},
	{
		name: "tulips",
		price: 250,
		isActive: true
	}

];

// app.get(<end point>, <function for req and res>);


app.get('/', (req, res) => {
	res.send(`Hello from my first expressJS API`)
	// optional to identify status code: res.status(200).send(`Hello from my first expressJS API`)
});


// MINI-ACTIVITY
app.get('/greeting', (req, res) => {
	res.send(`Hello from Batch182-Alarcon`)
});


//retrieval of mock database
app.get('/users', (req, res) =>{
	//res.send already stringifies for you
	res.send(users);
	//res.json(users) (Option)
});


//Post
// format app.post('',());

app.post('/users',(req, res) =>{
	console.log(req.body);
	
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	// push newuser into users array
	users.push(newUser);
	console.log(users);

	//send the updated users array in the Client
	res.send(users);
});


//Delete
// format app.delete('',());

app.delete('/users',(req, res) =>{
	console.log(req.body);

	users.pop();
	console.log(users);

	//send the updated users array in the Client
	res.send(users);
});

//PUT METHOD
// format app.put('',());
// app.put('/users/:index' (where index = object_id in MongoDB as example)
//localhose:4000/users/0



app.put('/users/:index',(req, res) =>{
	console.log(req.body);

	// an object that contains the value of URL params (object_id)
	console.log(req.params);

	//parseInt the value of the number coming from req.params
	let index = parseInt(req.params.index)


	users[index].password = req.body.password;


	res.send(users)[index];
});


// MINI-ACTIVITY

app.put('/users/update/:index',(req, res) =>{

	let index = parseInt(req.params.index);


	users[index].username = req.body.username;


	res.send(users)[index];
});


//Retrieval of single user
app.get('/users/getSingleUser/:index', (req, res) =>{

	console.log(req.params)
	let index = parseInt(req.params.index)
	console.log(index)

	res.send(users[index]);
	console.log(users[index]);
});

// ---------------------------------------------------
//--------A C T I V I T Y [ 3 3 ]  S O L U T I O N---- 
//----------------------------------------------------

//--------------ITEMS----------------------------------


// ------------------GET ALL ITEMS---------------------
app.get('/items', (req, res) =>{
	res.send(items);

});

//------------------CREATE ITEM-----------------------



app.post('/items',(req, res) =>{
	console.log(req.body);
	
	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	// push newuser into users array
	items.push(newItem);
	console.log(items);

	//send the updated users array in the Client
	res.send(items);
});







//------------------UPDATE ITEM----------------------

app.put('/items/:index',(req, res) =>{

	let index = parseInt(req.params.index);


	items[index].price = req.body.price;


	res.send(items)[index];
});


// ---------------------------------------------------
//--------A C T I V I T Y [ 3 4 ]  S O L U T I O N---- 
//----------------------------------------------------


/*
Activity #2
>> Create a new route with '/items/getSingleItem/:index' endpoint.
    >> This route should allow us to GET the details of a single item.
        -Pass the index number of the item you want to get via the url as url params in your postman.
        -http://localhost:4000/items/getSingleItem/<indexNumberOfItem>
   
    >> Send the request and back to your api
        -get the data from the url params.
        -send the particular item from our array using its index to the client.


*/

app.get('/items/getSingleItem/:index', (req, res) =>{

	console.log(req.params)
	let index = parseInt(req.params.index)
	console.log(index)

	res.send(items[index]);
	console.log(items[index]);
});

/*

>> Create a new route to update an item with '/items/archive/:index' endpoint.    (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to de-activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
        -send the updated item in the client


*/

app.put('/items/archive/:index',(req, res) =>{

	let index = parseInt(req.params.index);


	items[index].isActive =  false

	res.send(items[index]);

});

/*


>> Create a new route to update an item with '/items/activate/:index' endpoint. (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
        -send the updated item in the client


*/

app.put('/items/activate/:index',(req, res) =>{

	let index = parseInt(req.params.index);


	items[index].isActive =  true

	res.send(items[index]);

});


/*



>> Create a new collection in Postman called s34-activity
>> Save the Postman collection in your s33-s34 folder
*/



// port 

app.listen(port, () => console.log(`Server is running at port ${port}`))